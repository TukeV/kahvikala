# Kahvikala (coffeefish) #

Kahvikala is my little sailfish app made for jolla phone. It acts as an remote controller to my [coffee maker](https://bitbucket.org/TukeV/coffee-flask/overview).

It is not not my finest work and definitely not something I'm particularly proud of, but at least it gets the job done.