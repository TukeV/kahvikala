# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = kahvikala

CONFIG += sailfishapp

SOURCES += src/kahvikala.cpp

OTHER_FILES += qml/kahvikala.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    rpm/kahvikala.changes.in \
    rpm/kahvikala.spec \
    rpm/kahvikala.yaml \
    translations/*.ts \
    kahvikala.desktop

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n
TRANSLATIONS += translations/kahvikala-de.ts

HEADERS +=

DISTFILES += \
    qml/pages/SettingsDialog.qml

RESOURCES += \
    resources.qrc

