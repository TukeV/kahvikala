/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import QtQml 2.0
import QtQuick.LocalStorage 2.0

Page {

    property string username: ""
    property string password: ""
    property string address: ""

    id: page

    Component.onCompleted: {
        var db = LocalStorage.openDatabaseSync("kahvikala", "1.0", "Simple DB", 100);
        db.transaction(function(tx){
            tx.executeSql('CREATE TABLE IF NOT EXISTS Users(username TEXT, password TEXT, address TEXT)');
            var users = tx.executeSql("Select * FROM Users");
            page.username = users.rows.item(0).username;
            page.password = users.rows.item(0).password;
            page.address = users.rows.item(0).address;
        });
        ref.refreshStatus()
    }
    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            id: pull
            MenuItem {
                text: qsTr("Settings")
                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("SettingsDialog.qml"),
                                                {
                                                    "uname" : page.username,
                                                    "upwd":  page.password,
                                                    "address" : page.address
                                                });
                    dialog.onOpened
                    dialog.accepted.connect(function(){
                        page.username = dialog.uname;
                        page.password = dialog.upwd;
                        page.address = dialog.address;
                        var db = LocalStorage.openDatabaseSync("kahvikala", "1.0", "Simple DB", 100);
                        db.transaction(function(tx){
                            tx.executeSql('DELETE FROM Users');
                            tx.executeSql('INSERT INTO Users VALUES(?, ?, ?)', [ dialog.uname, dialog.upwd, dialog.address]);
                        });
                    });
                }
            }
            MenuItem {
                text: qsTr("New Timer")
                id: dtm
                onClicked: {
                    dtm.datetime = "!";
                    var dialog = pageStack.push(datepicker);
                }

                property string datetime

                Component {
                    id: datepicker
                    DatePickerDialog {
                        id: dp
                        acceptDestination: timepicker
                        date: new Date()
                        onAccepted: {
                            if(dtm.datetime != "") dtm.datetime = "";
                            dtm.datetime += dp.year + "-" + dp.month + "-" + dp.day + " ";
                        }
                    }
                }
                Component{
                    id: timepicker
                    TimePickerDialog {
                        id: tp
                        acceptDestination: page
                        acceptDestinationAction: PageStackAction.Pop
                        hour: new Date().getHours()
                        minute: new Date().getMinutes()
                        onAccepted: {
                            dtm.datetime += tp.hour + ":" + tp.minute;
                            dtm.setTimer(dtm.datetime);
                        }
                    }

                }
                function setTimer(str){
                    busy.running = true;
                    console.log(str);
                    var xhr = new XMLHttpRequest();
                    var params = "datetime=" + str;
                    console.log(params);
                    xhr.open("POST", "http://" + page.address + ":5000/timer", true);
                    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                    xhr.setRequestHeader("Authorization", "Basic " + Qt.btoa(page.username + ":" + page.password));
                    xhr.onreadystatechange = function(){
                        if(xhr.readyState == 4){
                            var x = xhr.status
                            if(x == 200){
                                errorLabel.text = "";
                                console.log("Onnistui");
                                ref.refresh(true);
                                var resp = JSON.parse(xhr.responseText);
                                ref.powerUpdate(resp.power);
                                ref.timerUpdate(resp.timer);
                                ref.timeoutUpdate(resp.timeout);
                                ref.countdownUpdate();
                                ref.refreshStatus();
                            }else{
                                errorLabel.text = JSON.parse(xhr.responseText).ERROR;
                                console.log(x + "-" + errorLabel.text);
                                ref.refresh(false);
                                ref.refreshStatus();
                            }
                            busy.running = false;
                        }
                    }
                    xhr.send(params);
                }
            }
            MenuItem {
                text: qsTr(powerLabel.power === "True" ? "Switch off" : "Switch on")
                onClicked: {
                    if(powerLabel.power === "True"){
                        switcher("off");
                    }
                    else{
                        switcher("on");
                    }
                }
                function switcher(onoff){
                    busy.running = true;
                    var xhr = new XMLHttpRequest();
                    var params = "power=" + onoff;
                    console.log(params);
                    xhr.open("POST", "http://" + page.address + ":5000/power", true);
                    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                    xhr.setRequestHeader("Authorization", "Basic " + Qt.btoa(page.username + ":" + page.password));
                    xhr.onreadystatechange = function(){
                        if(xhr.readyState == 4){
                            var x = xhr.status
                            if(x == 200){
                                errorLabel.text = "";
                                console.log("Onnistuu");
                                ref.refresh(true);
                                var resp = JSON.parse(xhr.responseText);
                                ref.powerUpdate(resp.power);
                                ref.timerUpdate(resp.timer);
                                ref.timeoutUpdate(resp.timeout);
                                ref.countdownUpdate();
                                ref.refreshStatus();
                            }else{
                                errorLabel.text = JSON.parse(xhr.responseText).ERROR;
                                console.log(x + "-" + errorLabel.text);
                                ref.refresh(false);
                                ref.refreshStatus();
                            }
                            busy.running = false;
                        }
                    }
                    xhr.send(params);
                }
            }
            MenuItem {
                id: ref
                text: qsTr("Refresh")
                signal refresh(bool conStatus)
                signal powerUpdate(string status)
                signal timerUpdate(string status)
                signal timeoutUpdate(string status)
                signal countdownUpdate()
                onClicked: {
                    refreshStatus()
                }
                function refreshStatus(){
                    busy.running = true;
                    var xhr = new XMLHttpRequest();
                    xhr.open("GET", "http://" + page.address + ":5000/status", true);
                    xhr.setRequestHeader("Authorization", "Basic " + Qt.btoa(page.username + ":" + page.password));
                    xhr.onreadystatechange = function(){
                        if(xhr.readyState == 4){
                            var x = xhr.status
                            if(x == 200){
                                console.log("Onnistus");
                                refresh(true);
                                var resp = JSON.parse(xhr.responseText);
                                powerUpdate(resp.power);
                                timerUpdate(resp.timer);
                                timeoutUpdate(resp.timeout);
                                countdownUpdate();
                            }else{
                                errorLabel.text = JSON.parse(xhr.responseText).ERROR;
                                console.log(x + "-" + errorLabel.text);
                                refresh(false);
                            }
                            busy.running = false;
                        }
                    }
                    xhr.send();
                }
            }
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Main page")
            }
            Label {
                x: Theme.paddingLarge
                text: qsTr("Kahvikala")
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }
            Label{
                id: userLabel
                x: Theme.paddingLarge
                text: "Username: " + page.username
            }
            Label{
                id: ipLabel
                x: Theme.paddingLarge
                text: "Address: " + page.address
            }
            Label {
                id: serverLabel
                x: Theme.paddingLarge
                property bool server: false
                text: qsTr(server ? "Server is online" : "Server is offline")
                color: server ? Theme.highlightColor : Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeMedium
                Component.onCompleted: {
                    ref.refresh.connect(update)
                }
                function update(conStatus){
                    serverLabel.server = conStatus
                }
            }
            Label {
                id: powerLabel
                x: Theme.paddingLarge
                property string power: ""
                text: qsTr("Power: " + power)
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeMedium
                Component.onCompleted: {
                    ref.powerUpdate.connect(update)
                }
                function update(status){
                    powerLabel.power = status
                }
            }
            Label {
                id: timerLabel
                x: Theme.paddingLarge
                property string timer: ""
                text: qsTr("Timer: " + timerLabel.timer)
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeMedium
                Component.onCompleted: {
                    ref.timerUpdate.connect(update)
                }
                function update(status){
                    if(status === "None")
                        timerLabel.timer = "-";
                    else
                        timerLabel.timer = status;
                }

            }
            Label {
                id: timeoutLabel
                x: Theme.paddingLarge
                property string timeout: ""
                text: qsTr("Timeout: " + timeout)
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeMedium
                Component.onCompleted: {
                    ref.timeoutUpdate.connect(update)
                }
                function update(status){
                    if(status === "None"){
                        timeoutLabel.timeout = "-"
                    }else{
                        timeoutLabel.timeout = status
                    }
                }
            }
            Label {
                id: countdownLabel
                x: Theme.paddingLarge
                y: Theme.paddingSmall
                property string toWhat: ""
                property string pre: "Time left " + toWhat
                text: qsTr("")
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeLarge
                signal resetcd(string date);
                Component.onCompleted: {
                    ref.countdownUpdate.connect(update)
                }
                function update(){
                    if(powerLabel.power === "True"){
                        countdownLabel.toWhat = "to timeout:";
                        countdownLabel.text = pre;
                        countdownLabel.resetcd(timeoutLabel.timeout);
                    }
                    else if(powerLabel.power === "False" && timerLabel.timer !== "None"){
                        countdownLabel.toWhat = "in timer:";
                        countdownLabel.text = pre;
                        countdownLabel.resetcd(timerLabel.timer);
                    }else{
                        countdownLabel.text = "";
                        countdownLabel.resetcd("");
                    }
                }
            }
            Label {
                id: countdownItem
                x: Theme.paddingLarge
                y: Theme.paddingSmall
                property string timedTo: ""
                text: qsTr("")
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeHuge
                Component.onCompleted: {
                    countdownLabel.resetcd.connect(update);
                }
                function update(dt){
                    if(dt === ""){
                        timer.stop();
                        countdownItem.text = "";
                    }else{
                        timedTo = dt;
                        timer.running = true;
                        timer.repeat = true;
                        timer.restart();
                    }
                }
                function updateTimer(){
                    var dt = Date.fromLocaleString(Qt.locale(), countdownItem.timedTo, "yyyy-MM-dd hh:mm:ss");
                    var tod = new Date()
                    var dif = dt.getTime() - tod.getTime();

                    x =  dif / 1000;
                    var sec = Math.floor(x % 60);
                    x /= 60;
                    var min = Math.floor(x % 60);
                    x /= 60;
                    var h = Math.floor(x);

                    var str = h + ":" + min + ":" + sec;

                    console.log("\n\t get:" + dt +
                                "\n\t tod:" + tod +
                                "\n\t dif:" + str);

                    if(dif > 0){
                        text = str;
                        console.log(dif);
                    }else{
                        text = "";
                        timer.stop();
                    }
                }
                Timer{
                    id: timer;
                    running: false;
                    repeat: false;
                    onTriggered: countdownItem.updateTimer();
                }
            }
            BusyIndicator{
                id: busy
                running: false
                x: Theme.paddingLarge
                y: Theme.paddingSmall
            }
            Label{
                id: errorLabel
                x: Theme.paddingLarge
                y: Theme.paddingSmall
                text: ""
                color: "red"
            }
        }
    }
}


