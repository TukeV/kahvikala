import QtQuick 2.2
import Sailfish.Silica 1.0

Dialog {
    property string uname
    property string upwd
    property string address
    Column{
        width: parent.width
        DialogHeader {
            title: "Settings"
        }
        TextField {
            id: nameField
            width: parent.width
            placeholderText: "username"
            label: "Username"
        }
        TextField {
            id: passwordField
            width: parent.width
            placeholderText: "password"
            label: "Password"
            echoMode: TextInput.Password
        }

        TextField {
            id: addressField
            width: parent.width
            placeholderText: "address"
            label: "Server address"
        }
    }
    onOpened: {
        if(uname != "")
            nameField.text = uname
        if(upwd != "")
            passwordField.text = upwd
        if(address != "")
            addressField.text = address
    }
    onDone: {
        if (result == DialogResult.Accepted) {
            uname = nameField.text
            upwd = passwordField.text
            address = addressField.text
        }
    }
}

